import {ResponseType} from "./ResponseType";

/**
 * Возможно придется переделать все это в классы
 */
export interface DataResponse<T> extends ResponseType {

  /**
   * Списочный результат данных.
   */
  data: T;

}
