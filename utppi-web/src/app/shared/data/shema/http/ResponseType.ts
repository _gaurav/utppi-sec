import {StatusType} from "../../enums/StatusType";
import {ResponseMessage} from "./ResponseMessage";

export interface ResponseType {
  status: StatusType;

  messages: Array<ResponseMessage>;
}
