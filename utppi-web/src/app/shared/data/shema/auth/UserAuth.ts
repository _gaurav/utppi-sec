import {Role} from "./Role";

export interface UserAuth {
  id: number;
  email: string;
  password: string;
  name: string;
  nikName: string;
  family: string;
  city: string;
  organization: string;
  code: number;
  dateAdded: Date;
  isActive: boolean;
  role: Role;
}
