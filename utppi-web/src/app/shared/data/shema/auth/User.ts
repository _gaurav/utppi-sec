import {Role} from "./Role";

export interface User {
  id: number;
  name: string;
  family: string;
  nikName: string;
  token: string;
  city: string;
  organization: string;
  code: number;
  authId: number;
  dateAdded: Date;
  isActive: boolean;
  role: Role;
}
