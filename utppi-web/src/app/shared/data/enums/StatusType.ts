export enum StatusType {
  SUCCESSFUL= "SUCCESSFUL",
  WARNING = "WARNING",
  ERROR = "ERROR"
}
