/**
 * Сессия.
 */
export class Session {

  /**
   * Идентификатор пользователя.
   */
  private tenantId: string;

  /**
   * Идентификатор приложения.
   */
  private applicationId: string;

  /**
   * Идентификатор пользователя для сессии.
   */
  private userId: string;

  constructor(tenantId: string, applicationId: string, userId: string) {
    this.tenantId = tenantId;
    this.applicationId = applicationId;
    this.userId = userId;
  }
}
