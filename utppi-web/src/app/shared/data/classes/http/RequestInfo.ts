import {MetaInfo} from "./MetaInfo";

/**
 * Дополнительныая информация в запросе
 */
export class RequestInfo {
  private metaInfo: MetaInfo | undefined;

  constructor(metaInfo: MetaInfo | undefined) {
    this.metaInfo = metaInfo;
  }
}
