/**
 * Мета информация.
 */
import {Session} from "./Session";

export class MetaInfo {
  /**
   * Метка времени.
   */
  private timestamp: number;

  /**
   * Сессия.
   */
  private session: Session;

  /**
   * Id канала.
   */
  private channelId: string;

  constructor(timestamp: number, session: Session, channelId: string) {
    this.timestamp = timestamp;
    this.session = session;
    this.channelId = channelId;
  }
}
