import {MetaInfo} from "./MetaInfo";
import {RequestInfo} from "./RequestInfo";

/**
 * Запрос с данными.
 *
 * @param <T> тип данных запроса
 */
export class DataRequest<T> extends RequestInfo {

  /**
   * Данные запроса.
   */
  private data: T;

  constructor(metaInfo: MetaInfo | undefined, data: T) {
    super(metaInfo);
    this.data = data;
  }
}
