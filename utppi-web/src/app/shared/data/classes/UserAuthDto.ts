import {RoleDto} from "./RoleDto";

export class UserAuthDto {
  private id: number | null;
  private email!: string;
  private password!: string;
  private name!: string;
  private family!: string;
  private nikName!: string;
  private city!: string;
  private organization!: string;
  private code!: number;
  private dateAdded!: Date;
  private isActive!: boolean;
  private role!: RoleDto;

  constructor() {
    this.id = null;
  }

  set _id(value: number | null) {
    this.id = value;
  }

  set _email(value: string) {
    this.email = value;
  }

  set _password(value: string) {
    this.password = value;
  }

  set _name(value: string) {
    this.name = value;
  }

  set _family(value: string) {
    this.family = value;
  }

  set _city(value: string) {
    this.city = value;
  }

  set _organization(value: string) {
    this.organization = value;
  }

  set _code(value: number) {
    this.code = value;
  }

  set _dateAdded(value: Date) {
    this.dateAdded = value;
  }

  set _isActive(value: boolean) {
    this.isActive = value;
  }

  set _role(value: RoleDto) {
    this.role = value;
  }

  set _nikName(value: string) {
    this.nikName = value;
  }

}
