export class RoleDto {
  private id: number;
  private roleName: string;


  constructor(_id: number, _roleName: string) {
    this.id = _id;
    this.roleName = _roleName;
  }


  set _id(value: number) {
    this.id = value;
  }

  set _roleName(value: string) {
    this.roleName = value;
  }

}

