import { Component, OnInit } from '@angular/core';

/**
 * Этот компонет можно будет удалить
 * Меню будем деалть по другому
 */
@Component({
  selector: 'app-home-menu',
  templateUrl: './home-menu.component.html',
  styleUrls: ['./home-menu.component.scss']
})
export class HomeMenuComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
