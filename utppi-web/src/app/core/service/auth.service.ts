import { Injectable } from '@angular/core';
import {UserAuthDto} from "../../shared/data/classes/UserAuthDto";
import {AuthHttpService} from "../http/auth-http.service";
import {DataResponse} from "../../shared/data/shema/http/DataResponse";
import {User} from "../../shared/data/shema/auth/User";
import {StatusType} from "../../shared/data/enums/StatusType";
import {map, Observable, switchMap} from "rxjs";
import {UserAuth} from "../../shared/data/shema/auth/UserAuth";
import {CookieStateService} from "./cookie-state.service";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  static readonly ROLE_ADMIN = "ROLE_ADMIN";

  constructor(
    private authHttpService: AuthHttpService,
    private cookieStateService: CookieStateService
  ) { }


  /*
   register(userAuth: UserAuthDto) {
    this.authHttpService.registerHttp(userAuth).subscribe(
      (resultService: DataResponse<User>) => {
        console.log("data is received");
        console.log("status = " + resultService.status);
        if (resultService.status === StatusType.SUCCESSFUL) {
          console.log("is SUCCESSFUL");
        }
      });

  }

   */

  register(userAuth: UserAuthDto): Observable<User | string> {
    return this.authHttpService.registerHttp(userAuth).pipe(
      map((result: DataResponse<User>) => {
            if (result.status === StatusType.SUCCESSFUL) {
              console.log("is SUCCESSFUL");
              console.log("StatusType = ", result.status);

              return result.data;
            } else {
              console.log("StatusType = ", result.status);
              console.log("messages = ", result.messages.length);
              console.log("messages = ", result.messages[0].message);
              if (result.messages.length && result.messages.length > 0) {
                return result.messages[0].message;
              }
              return "Не известная ошибка";
            }
        })
    );
  }

  activate(email: string): Observable<UserAuth | string> {
    return this.authHttpService.activateHttp(email).pipe(
      map((result: DataResponse<UserAuth>) => {
        if (result.status === StatusType.SUCCESSFUL) {
          console.log("is SUCCESSFUL");
          return result.data;
        } else {
          if (result.messages.length && result.messages.length > 0) {
            return result.messages[0].message;
          }
          return "Не известная ошибка";
        }
      })
    );
  }

  login(userAuth: UserAuthDto): Observable<User | string> {
    return this.authHttpService.loginHttp(userAuth).pipe(
      map((result: DataResponse<User>) => {
        if (result.status === StatusType.SUCCESSFUL) {
          console.log("is SUCCESSFUL");
          console.log("StatusType = ", result.status);
          let token = result.data.token;
          if (token && token.length > 0) {
            console.log("token = ", token);
            this.cookieStateService.setToken(token);
            let role = result.data.role.roleName;
            let role_admin = false;
            if (role && role.length > 0) {
              role_admin = (role === AuthService.ROLE_ADMIN);
            }
            this.cookieStateService.setRoleAdmin(role_admin);
          }
          return result.data;
        } else {
          console.log("StatusType = ", result.status);
          console.log("messages = ", result.messages.length);
          console.log("messages = ", result.messages[0].message);
          if (result.messages.length && result.messages.length > 0) {
            return result.messages[0].message;
          }
          return "Не известная ошибка";
        }
      })
    );
  }



  // getManufacture(id: number): Observable<Manufacture> {
  //   return this.sharedDataService.tokens.pipe(
  //     switchMap((result) => this.manufactureService.getManufacture(result, id)),
  //     map((resultService: DataResponse<Manufacture>) => {
  //       if (resultService.status === StatusType.SUCCESSFUL) {
  //         return resultService.data;
  //       } else {
  //         return {"id":0, "name":""};
  //       }
  //     })
  //   );
  // }


}
