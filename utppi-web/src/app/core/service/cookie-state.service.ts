import { Injectable } from '@angular/core';
import {CookieService} from "ngx-cookie-service";

@Injectable({
  providedIn: 'root'
})
export class CookieStateService {

  static readonly LOGGED_NAME = "Logged_state";
  static readonly TOKEN_NAME = "utppi_token";
  static readonly ADMIN_STATE = "admin_state";

  // флаг авторизации, т.е. текущий сеанс авторизован
  isLoggedIn = false;
  // флаг администратора - если зашли под админом
  isAdminIn = false;

  constructor(
    private cookieService: CookieService
  )
  {

    this.initData();
    // console.log("CookieState isLoggedIn = " + this.isLoggedIn)
  }

  /**
   * Ининциализация всех переменных
   */
  initData() {
    let loggedIn = this.cookieService.get( CookieStateService.LOGGED_NAME);
    if (loggedIn && loggedIn != "") {
      console.log("loggedIn = not null");
      this.isLoggedIn = JSON.parse(loggedIn);
    }

    // this.isLoggedIn =

  }

  private setLogged(isLogged: boolean) {
    this.cookieService.set( CookieStateService.LOGGED_NAME, String(isLogged));
  }

  setToken(token: string) {
    this.cookieService.set( CookieStateService.TOKEN_NAME, token);
    this.setLogged(true);
  }

  setRoleAdmin(roleAdmin: boolean) {
    this.cookieService.set( CookieStateService.ADMIN_STATE, String(roleAdmin));
  }

  getLogged(): boolean {
    return this.isLoggedIn;
    // this.user = JSON.parse(this.cookieService.get('user'));
  }

}
