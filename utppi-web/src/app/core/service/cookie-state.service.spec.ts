import { TestBed } from '@angular/core/testing';

import { CookieStateService } from './cookie-state.service';

describe('CookieStateService', () => {
  let service: CookieStateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CookieStateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
