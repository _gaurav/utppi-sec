import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class SharedDataService {

  private token = new BehaviorSubject<any>(null);
  private codeValue = new BehaviorSubject<number>(0);
  private email = new BehaviorSubject<string>("");

  token$ = this.token.asObservable();
  codeValue$ = this.codeValue.asObservable();
  email$ = this.email.asObservable();

  constructor() { }

  sendToken(token: string){
    this.token.next(token);
  }

  sendCodeValue(codeValue: number){
    this.codeValue.next(codeValue);
  }

  sendEmail(email: string){
    this.email.next(email);
  }

}
