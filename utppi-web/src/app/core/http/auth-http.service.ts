import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {UserAuthDto} from "../../shared/data/classes/UserAuthDto";
import {Observable} from "rxjs";
import {User} from "../../shared/data/shema/auth/User";
import {DataResponse} from "../../shared/data/shema/http/DataResponse";
import {DataRequest} from "../../shared/data/classes/http/DataRequest";
import {UserAuth} from "../../shared/data/shema/auth/UserAuth";

@Injectable({
  providedIn: 'root'
})
export class AuthHttpService {

  API_URL = environment.authUrl + '/api/v1/user'


  constructor(private http: HttpClient) { }

  // registerHttp(userAuth: UserAuthDto): Observable<DataResponse<User>> {
  registerHttp(userAuth: UserAuthDto) {
    console.log("AuthHttpService is register");
    let dataRequest = new DataRequest<UserAuthDto>(undefined, userAuth);
    // const  headers: HttpHeaders = new HttpHeaders();
    return this.http.post<DataResponse<User>>(this.API_URL + '/register', dataRequest);
    // return this.http.post<DataResponse<User>>(this.API_URL + '/register', dataRequest, {headers});

    // return this.http.get<DataResponse<Manufacture>>(this.API_URL + '/get/' + id, {headers});
  }

  activateHttp(email: string) {
    console.log("AuthHttpService is register");
    const  headers: HttpHeaders = new HttpHeaders();
    return this.http.put<DataResponse<UserAuth>>(this.API_URL + '/activate/' + email, {headers});
  }

  loginHttp(userAuth: UserAuthDto) {
    console.log("AuthHttpService is register");
    let dataRequest = new DataRequest<UserAuthDto>(undefined, userAuth);
    const  headers: HttpHeaders = new HttpHeaders();
    return this.http.post<DataResponse<User>>(this.API_URL + '/login', dataRequest, {headers});
  }


}
