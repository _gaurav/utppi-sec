import { Component, OnInit } from '@angular/core';
import {UserAuth} from "../../../../shared/data/shema/auth/UserAuth";
import {Role} from "../../../../shared/data/shema/auth/Role";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserAuthDto} from "../../../../shared/data/classes/UserAuthDto";
import {RoleDto} from "../../../../shared/data/classes/RoleDto";
import {User} from "../../../../shared/data/shema/auth/User";
import {AuthService} from "../../../../core/service/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm!: FormGroup;
  userAuthDto!: UserAuthDto;
  message!: string;
  display = 'none';

  userAuth: UserAuth = {
    id: 0,
    email: "",
    password: "",
    name: "",
    family: "",
    nikName: "",
    city: "",
    organization: "",
    code: 0,
    dateAdded: new Date(),
    isActive: false,
    role: {
      id: 0,
      roleName: ""
    }
  };

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {
    this.createForm();
  }

  ngOnInit(): void {
    console.log("LoginComponent");
  }

  readAndValidationData() {
    this.userAuthDto = new UserAuthDto();
    // email
    let email = this.loginForm.get('email')?.value;
    if (email && email.trim().length > 0) {
      this.userAuthDto._email = email;
    } else {
      this.message = 'Заполните поле email';
    }
    // password
    let password = this.loginForm.get('password')?.value;
    if (password && password.trim().length > 0) {
      this.userAuthDto._password = password;
    } else {
      this.message = 'Заполните поле Пароль';
    }
    this.userAuthDto._id = null;
    this.userAuthDto._code = 0;
    this.userAuthDto._nikName = "none";
    this.userAuthDto._name = "none";
    this.userAuthDto._family = "none";
    this.userAuthDto._organization = "none";
    this.userAuthDto._city = "none";
    this.userAuthDto._isActive = false;
    this.userAuthDto._dateAdded = new Date();
    // Пока не поддтверждена записиь роль назначим ему такую, потом сменим
    this.userAuthDto._role = new RoleDto(2, "ROLE_UNDEFINED");

    if (this.message && this.message.length > 0) {
      this.openModal();
    } else {
      console.log("сообщений нет - go reg");
      this.login(this.userAuthDto);
    }
  }

  login(userAuth: UserAuthDto) {
    console.log("start reg");
    this.authService.login(userAuth).subscribe(
      (result: User | string) => {
        if (typeof result === "string") {
          console.log("mes = ", result);
          this.message = result;
          this.openModal();
        } else {
          this.router.navigate(['quiz']);
        }
      })

  }


  createForm() {
    this.loginForm = this.formBuilder.group({
      email: [null, [Validators.required,
        Validators.email]],
      password: [null, [Validators.required,
        Validators.minLength(5)]],
    });
  }


  /**
   * Сохраним в куках токин.
   * Пока не получилось - выдает ошибку, cookieService, надо разобраться
   * @param curToken
   */
  setCookie(curToken: string){
    // Реализуем через спец севрис

  }

  openModal(): void {
    this.display = 'block';
  }

  onCloseHandled(): void {
    this.display = 'none';
    this.message = ''
  }

}
