import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserAuthDto} from "../../../../shared/data/classes/UserAuthDto";
import {RoleDto} from "../../../../shared/data/classes/RoleDto";
import {AuthService} from "../../../../core/service/auth.service";
import {DataResponse} from "../../../../shared/data/shema/http/DataResponse";
import {User} from "../../../../shared/data/shema/auth/User";
import {StatusType} from "../../../../shared/data/enums/StatusType";
import {Router} from "@angular/router";
import {SharedDataService} from "../../../../core/service/shared-data.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  regForm!: FormGroup;
  display = 'none';
  message!: string;
  userAuthDto!: UserAuthDto;


  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private sharedDataService: SharedDataService,
    private router: Router
  ) {
    this.createForm();
  }

  ngOnInit(): void {
    console.log("RegisterComponent ngOnInit");
  }

  createForm() {
    this.regForm = this.formBuilder.group({
      name: [null, [Validators.required,
        Validators.maxLength(250)]],
      family: [null, Validators.required],
      nikName: [null, Validators.required],
      city: [null, Validators.required],
      organization: [null, Validators.required],
      email: [null, [Validators.required,
        Validators.email]],
      password: [null, [Validators.required,
        Validators.minLength(5)]],
    });
  }

  /**
   * Чтение и проверка данных с формы
   */
  readAndValidationData() {
    this.userAuthDto = new UserAuthDto();

    let name = this.regForm.get('name')?.value;
    if (name && name.trim().length > 0) {
      this.userAuthDto._name = name;
    } else {
      this.message = 'Заполните поле Имя';
    }
    // family
    let family = this.regForm.get('family')?.value;
    if (family && family.trim().length > 0) {
      this.userAuthDto._family = family;
    } else {
      this.message = 'Заполните поле Фамилия';
    }
    // nikName
    let nikName = this.regForm.get('nikName')?.value;
    if (nikName && nikName.trim().length > 0) {
      this.userAuthDto._nikName = nikName;
    } else {
      this.message = 'Заполните поле Фамилия';
    }
    // city
    let city = this.regForm.get('city')?.value;
    if (city && city.trim().length > 0) {
      this.userAuthDto._city = city;
    } else {
      this.message = 'Заполните поле Город';
    }
    // organization
    let organization = this.regForm.get('organization')?.value;
    if (organization && organization.trim().length > 0) {
      this.userAuthDto._organization = organization;
    } else {
      this.message = 'Заполните поле Организация';
    }
    // email
    let email = this.regForm.get('email')?.value;
    if (email && email.trim().length > 0) {
      this.userAuthDto._email = email;
    } else {
      this.message = 'Заполните поле email';
    }
    // password
    let password = this.regForm.get('password')?.value;
    if (password && password.trim().length > 0) {
      this.userAuthDto._password = password;
    } else {
      this.message = 'Заполните поле Пароль';
    }
    this.userAuthDto._id = null;
    this.userAuthDto._code = 0;
    this.userAuthDto._isActive = false;
    this.userAuthDto._dateAdded = new Date();
    // Пока не поддтверждена записиь роль назначим ему такую, потом сменим

    this.userAuthDto._role = new RoleDto(2, "ROLE_UNDEFINED");
    // this.userAuthDto._role = new RoleDto(2, "ROLE_USER");

    // TODO: может стоит выводить все сообщения в том числе и успешная регистрация
    if (this.message && this.message.length > 0) {
      this.openModal();
    } else {
      console.log("сообщений нет - go reg");
      this.sharedDataService.sendEmail(email);
      this.register(this.userAuthDto);
    }

    // this.mandoForm.get('name').value)

  }

  register(userAuth: UserAuthDto) {
    console.log("start reg");
    this.authService.register(userAuth).subscribe(
      (result: User | string) => {
        if (typeof result === "string") {
          console.log("mes = ", result);
          this.message = result;
          this.openModal();
          // TODO: Пока нет реалзиции email сервиса, имитируем получение кода 0
          this.sharedDataService.sendCodeValue(1234);
          this.router.navigate(['auth/confirm']);
        } else {
          console.log("code = ", result.code);
          this.sharedDataService.sendCodeValue(result.code);
          this.router.navigate(['auth/confirm']);
        }
      })
    // TODO: добавить ответ от сервера



    // this.authService.register(userAuth)
    //   .subscribe(
    //     (result: DataResponse<User>) => {
    //       console.log(" machineService data is received");
    //       console.log("status = " + result.status);
    //       console.log("result = ", result);
    //       if (result.status === StatusType.SUCCESSFUL) {
    //         console.log("StatusType.SUCCESSFUL");
    //         // this.machines = [];
    //         // this.machines.push(...result.listResult.list as Machine[]);
    //         // this.dataSource = new MatTableDataSource(this.machines);
    //       }
    //       console.log("MachineCatalogComponent данные получены");
    //     })
    //


    // this.auth.registerProxy(this.regForm.value).subscribe(data => {
    //   switch (data) {
    //     case 'SUCCESS':
    //       this.message = 'Регистрация прошла успешно!';
    //       console.log("this.message = " + this.message);
    //
    //       // Открываем окно ConfirmComponent подтверждение регистрации
    //       this.router.navigate(['confirm']);
    //       // this.renderComponentService.renderAuth(2);
    //       break;
    //
    //     case 'FAIL':
    //       this.message = 'Произошла ошибка сервера, пройдите регистрацию еще раз';
    //       break;
    //
    //     case 'PHONE_EXISTS':
    //       this.message = 'Данный номер телефона уже зарегистрирован';
    //       break;
    //
    //     case 'PASSWORD_INCORRECT':
    //       this.message = 'Неправильный пароль';
    //       break;
    //
    //     case 'CODE_REASON_EXISTS':
    //       this.message = 'Данный номер КПП уже зарегистрирован';
    //       break;
    //
    //     case 'IDENTIFICATION_NUMBER_EXISTS':
    //       this.message = 'Данный номер ИНН уже зарегистрирован';
    //       break;
    //
    //     case 'ALREADY_EXISTS':
    //       this.message = 'Аккаунт уже зарегистрирован';
    //       break;
    //   }
    //   this.openModal();
    // });

  }

  openModal(): void {
    this.display = 'block';
  }

  onCloseHandled(): void {
    this.display = 'none';
    this.message = ''
  }

}
