import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {SharedDataService} from "../../../../core/service/shared-data.service";
import {User} from "../../../../shared/data/shema/auth/User";
import {AuthService} from "../../../../core/service/auth.service";
import {Router} from "@angular/router";
import {UserAuth} from "../../../../shared/data/shema/auth/UserAuth";

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {

  confirmForm!: FormGroup;
  curCode: number = 0;
  curEmail = "";
  display = 'none';
  message!: string;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private sharedDataService: SharedDataService,
    private router: Router
  ) {
    this.createForm();
  }

  ngOnInit(): void {
    // TODO: оформить отписку
    this.sharedDataService.codeValue$.subscribe(
      result =>{
        if (result != null) {
          console.log("код изменен = ", result);
          this.curCode = result;
        }
      });

    this.sharedDataService.email$.subscribe(
      result =>{
        if (result != null) {
          console.log("curEmail изменен = ", result);
          this.curEmail = result;
        }
      });

  }

  createForm() {
    this.confirmForm = this.formBuilder.group({
      code: [null, [Validators.required,
        Validators.pattern(/^[0-9]*$/),
        Validators.minLength(4),
        Validators.maxLength(4)]]
    });
  }


  /**
   * Проверка введенного кода с полученным
   */
  checkCode() {
    let code = this.confirmForm.get('code')?.value;
    if (code && code.trim().length > 0) {
      console.log("code = ", code);
      if (this.curCode == code) {

        this.authService.activate(this.curEmail).subscribe(
          (result: UserAuth | string) => {
            if (typeof result === "string") {
              console.log("mes = ", result);
              this.message = result;
              this.openModal();
            } else {
              console.log("code = ", result.code);
              this.sharedDataService.sendCodeValue(result.code);
              this.message = "Аккаунт активирован";
              this.openModal();
              this.router.navigate(['auth/login']);
            }
          })

        this.message = 'Аккаунт активирован';
        console.log("коды совпали  = ", code );
      } else {
        this.message = 'Ошибка активации';
        console.log("коды не совпали  = ", code );
        console.log("коды не совпали this = ", this.curCode );
      }
    }
    if (this.message && this.message.length > 0) {
      this.openModal();
    }
  }

  openModal(): void {
    this.display = 'block';
  }

  onCloseHandled(): void {
    this.display = 'none';
  }


}
