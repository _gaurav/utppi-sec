import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeQuizComponent } from './page/home-quiz/home-quiz.component';
import {QuizRoutingModule} from "./quiz-routing.module";



@NgModule({
  declarations: [
    HomeQuizComponent
  ],
  imports: [
    CommonModule,
    QuizRoutingModule
  ]
})
export class QuizModule { }
