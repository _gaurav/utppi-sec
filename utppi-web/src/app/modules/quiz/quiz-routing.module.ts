import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {HomeMenuComponent} from "../../shared/components/menu/home-menu/home-menu.component";
import {LoginComponent} from "../auth/page/login/login.component";
import {HomeQuizComponent} from "./page/home-quiz/home-quiz.component";
import {AuthGuard} from "../../core/guard/auth.guard";


const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: '',
    // component: HomeMenuComponent, тут будет layout
    children: [
      {
        path: 'home',
        component: HomeQuizComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'reg',
        // component: RegisterComponent
      },
      {
        path: 'conf',
        // component: ConfirmComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuizRoutingModule {
}
