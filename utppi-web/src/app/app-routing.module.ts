import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeMenuComponent} from "./shared/components/menu/home-menu/home-menu.component";
import {LoginComponent} from "./modules/auth/page/login/login.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'quiz',
    pathMatch: 'full'
  },

  {
    path: 'quiz',
    // component: HomeQuiz - LoginComponent, добавить Gaurd
    loadChildren: () =>
      import('./modules/quiz/quiz.module').then(m => m.QuizModule)
  },

  {
    path: 'auth',
    // component: LoginComponent,
    loadChildren: () =>
      import('./modules/auth/auth.module').then(m => m.AuthModule)
  },

  // {
  //   path: 'admin-catalog',
  //   component: AdminCatalogMenuComponent,
  //   loadChildren: () =>
  //     import('./modules/admin-catalog/admin-catalog.module').then(m => m.AdminCatalogModule)
  // },
  {
    // исправить на LayoutComponent , HomeMenuComponent удалить или перенести все в отдельный
    // модуль а тут оставить только error
    path: '', component: HomeMenuComponent, children: [
      // {path: '', component: MainUserComponent},
      // {path: 'data', component: MyDataComponent, canActivate: [AuthGuard]},
      // {path: 'current_orders', component: MyOrdersComponent, canActivate: [AuthGuard]},
      // {path: 'history_orders', component: HistoryOrderComponent, canActivate: [AuthGuard]},
      // {path: 'sto', component: MyStoComponent, canActivate: [AuthGuard]},
      // {path: 'techpark', component: MyTechParkComponent, canActivate: [AuthGuard]},
      // {path: 'insurance', component: InsuranceComponent, canActivate: [AuthGuard]},
      // {path: 'number_search', component: SearchPartsComponent},
      // {path: 'list_search', component: ListSearchComponent},
      // {path: 'offers', component: SpecialOffersComponent, canActivate: [AuthGuard]},
      // {path: 'inventories/:type/:id', component: DetailsCardComponent},

      // {path: '**', redirectTo: '', pathMatch: 'full'}
    ],

  }];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
