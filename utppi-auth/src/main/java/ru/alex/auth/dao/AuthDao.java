package ru.alex.auth.dao;

import ru.alex.auth.entity.Auth;
import ru.alex.common.service.CrudService;

/**
 * Здесь будут добавлены специфичные методы для данной таблицы
 * Наверное этот интерфейс уже лишний
 */
public interface AuthDao extends CrudService<Auth, Long> {

}
