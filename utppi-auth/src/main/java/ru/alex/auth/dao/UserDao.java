package ru.alex.auth.dao;

import ru.alex.auth.entity.User;
import ru.alex.common.service.CrudService;

/**
 * Здесь будут добавлены специфичные методы для данной таблицы
 * Наверное этот интерфейс уже лишний
 */
public interface UserDao extends CrudService<User, Long> {
    //    void deleteForId(String id);
//
//    void deleteByName(String name);
//
    // получение объекта по имени
//    ManufactureEntity getByName(String name);

}
