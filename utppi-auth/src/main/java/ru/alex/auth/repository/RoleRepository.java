package ru.alex.auth.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.alex.auth.entity.Role;

/**
 * Здесь будут добавлены специфичные методы для данной таблицы,
 * все стандартные методы уже добавлены автоматически
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {


}
