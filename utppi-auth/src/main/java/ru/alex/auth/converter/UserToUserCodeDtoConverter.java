package ru.alex.auth.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.alex.auth.entity.Role;
import ru.alex.auth.entity.User;
import ru.alex.common.dto.auth.UserAuthDto;
import ru.alex.common.dto.auth.RoleDto;
import ru.alex.common.dto.converter.Converter;
import ru.alex.common.dto.converter.Impl.AbstractIdentifiableConverter;

/**
 * Конвертация в объект без пароля
 */
@Component
public class UserToUserCodeDtoConverter extends AbstractIdentifiableConverter<Long,
        User, UserAuthDto> {

    private final Converter<Role, RoleDto> roleToRoleDtoConverter;

    @Autowired
    public UserToUserCodeDtoConverter(
            RoleToRoleDtoConverter roleToRoleDtoConverter) {
        super();
        this.roleToRoleDtoConverter = roleToRoleDtoConverter;
    }

    @Override
    public UserAuthDto convert(User input) {
        final UserAuthDto output = super.convert(input);
        output.setName(input.getName());
        output.setFamily(input.getFamily());
        output.setNikName(input.getNikName());
        output.setIsActive(input.getIsActive());
        output.setDateAdded(input.getDateAdded());
        output.setCity(input.getCity());
        output.setOrganization(input.getOrganization());
        output.setCode(input.getCode());
        output.setRole(roleToRoleDtoConverter.convert(
                input.getRole()));
        return output;
    }

    @Override
    protected UserAuthDto createOutput() {
        return new UserAuthDto();
    }

}
