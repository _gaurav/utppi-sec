package ru.alex.auth.converter;

import ru.alex.auth.entity.Auth;
import ru.alex.common.dto.auth.AuthDto;
import ru.alex.common.dto.converter.Impl.AbstractIdentifiableConverter;

public class AuthToAuthDtoConverter extends AbstractIdentifiableConverter<Long,
        Auth, AuthDto> {

    @Override
    public AuthDto convert(Auth input) {
        final AuthDto output = super.convert(input);
        output.setEmail(input.getEmail());
        output.setPassword(input.getPassword());
        return output;
    }

    @Override
    protected AuthDto createOutput() {
        return new AuthDto();
    }

}
