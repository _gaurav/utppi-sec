package ru.alex.auth.converter;

import org.springframework.stereotype.Component;
import ru.alex.auth.entity.Auth;
import ru.alex.common.dto.auth.UserAuthDto;
import ru.alex.common.dto.converter.Impl.AbstractIdentifiableConverter;

@Component
public class UserAuthDtoToAuthConverter extends AbstractIdentifiableConverter<Long,
        UserAuthDto, Auth> {

    @Override
    public Auth convert(UserAuthDto input) {
        final Auth output = super.convert(input);
        output.setEmail(input.getEmail());
        output.setPassword(input.getPassword());
        return output;
    }

    @Override
    protected Auth createOutput() {
        return new Auth();
    }

}
