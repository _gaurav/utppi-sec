package ru.alex.auth.converter;

import org.springframework.stereotype.Component;
import ru.alex.auth.entity.Role;
import ru.alex.common.dto.auth.RoleDto;
import ru.alex.common.dto.converter.Impl.AbstractIdentifiableConverter;

@Component
public class RoleToRoleDtoConverter extends AbstractIdentifiableConverter<Long,
        Role, RoleDto> {

    @Override
    public RoleDto convert(Role input) {
        final RoleDto output = super.convert(input);
        output.setRoleName(input.getRoleName());
        return output;
    }

    @Override
    protected RoleDto createOutput() {
        return new RoleDto();
    }

}
