package ru.alex.auth.converter;

import org.springframework.stereotype.Component;
import ru.alex.auth.entity.User;
import ru.alex.common.dto.auth.UserActivateDto;
import ru.alex.common.dto.converter.Impl.AbstractIdentifiableConverter;

/**
 * Конвертация в объект для активации
 */
@Component
public class UserToUserActivateDtoConverter extends AbstractIdentifiableConverter<Long,
        User, UserActivateDto> {

    @Override
    public UserActivateDto convert(User input) {
        final UserActivateDto output = super.convert(input);
        output.setIsActive(input.getIsActive());
        return output;
    }

    @Override
    protected UserActivateDto createOutput() {
        return new UserActivateDto();
    }

}
