package ru.alex.auth.converter;

import org.springframework.stereotype.Component;
import ru.alex.common.data.AuthConstants;
import ru.alex.common.dto.auth.EmailDto;
import ru.alex.common.dto.auth.UserAuthDto;
import ru.alex.common.dto.converter.Impl.AbstractIdentifiableConverter;

/**
 * Конвертация в объект DTO для email-сервиса
 * Передаем имя и код и там уже формируем Html
 */
@Component
public class UserAuthDtoToEmailDtoConverter extends AbstractIdentifiableConverter<Long,
        UserAuthDto, EmailDto> {

    @Override
    public EmailDto convert(UserAuthDto input) {
        final EmailDto output = super.convert(input);
        output.setTo(input.getEmail());
        output.setSubject(AuthConstants.EMAIL_SUBJECT);
        output.setBody(input.getName());
        output.setFullName(input.getFamily() + " " + input.getName());
        output.setCode(input.getCode());
        return output;
    }

    @Override
    protected EmailDto createOutput() {
        return new EmailDto();
    }

}
