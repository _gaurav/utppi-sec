package ru.alex.auth.converter;

import org.springframework.stereotype.Component;
import ru.alex.auth.entity.Role;
import ru.alex.common.dto.auth.RoleDto;
import ru.alex.common.dto.converter.Impl.AbstractIdentifiableConverter;

@Component
public class RoleDtoToRoleConverter extends AbstractIdentifiableConverter<Long,
        RoleDto, Role> {

    @Override
    public Role convert(RoleDto input) {
        final Role output = super.convert(input);
        output.setRoleName(input.getRoleName());
        return output;
    }

    @Override
    protected Role createOutput() {
        return new Role();
    }

}
