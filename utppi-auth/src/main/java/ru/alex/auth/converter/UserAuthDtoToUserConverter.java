package ru.alex.auth.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.alex.auth.entity.Role;
import ru.alex.auth.entity.User;
import ru.alex.common.dto.auth.RoleDto;
import ru.alex.common.dto.auth.UserAuthDto;
import ru.alex.common.dto.auth.UserDto;
import ru.alex.common.dto.converter.Converter;
import ru.alex.common.dto.converter.Impl.AbstractIdentifiableConverter;

@Component
public class UserAuthDtoToUserConverter extends AbstractIdentifiableConverter<Long,
        UserAuthDto, User> {

    private final Converter<RoleDto, Role> roleDtoToRoleConverter;

    @Autowired
    public UserAuthDtoToUserConverter(
            RoleDtoToRoleConverter roleDtoToRoleConverter) {
        super();
        this.roleDtoToRoleConverter = roleDtoToRoleConverter;
    }

    @Override
    public User convert(UserAuthDto input) {
        final User output = super.convert(input);
        output.setName(input.getName());
        output.setFamily(input.getFamily());
        output.setNikName(input.getNikName());
        output.setIsActive(input.getIsActive());
        output.setDateAdded(input.getDateAdded());
        output.setCity(input.getCity());
        output.setOrganization(input.getOrganization());
        output.setCode(input.getCode());
        output.setRole(roleDtoToRoleConverter.convert(
                input.getRole()));
        return output;
    }

    @Override
    protected User createOutput() {
        return new User();
    }

}
