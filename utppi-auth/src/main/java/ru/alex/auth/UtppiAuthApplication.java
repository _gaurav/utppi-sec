package ru.alex.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UtppiAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(UtppiAuthApplication.class, args);
    }

}
