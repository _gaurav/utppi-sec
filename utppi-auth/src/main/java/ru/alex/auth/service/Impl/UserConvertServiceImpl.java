package ru.alex.auth.service.Impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ru.alex.auth.converter.*;
import ru.alex.auth.entity.Auth;
import ru.alex.auth.entity.User;
import ru.alex.auth.security.JwtTokenProvider;
import ru.alex.auth.service.*;
import ru.alex.common.dto.auth.UserActivateDto;
import ru.alex.common.dto.auth.UserAuthDto;
import ru.alex.common.dto.auth.UserDto;
import ru.alex.common.dto.data.DataRequest;
import ru.alex.common.dto.data.DataResponse;
import ru.alex.common.enums.Result;
import ru.alex.common.enums.StatusType;
import ru.alex.common.utils.UserUtils;
import java.time.OffsetDateTime;
import java.util.Optional;

import static ru.alex.common.data.TokenConstants.HEADER_NAME;

@Slf4j
@AllArgsConstructor
@Service
public class UserConvertServiceImpl implements UserConvertService {

    private final UserToUserDtoConverter userToUserDtoConverter;
    private final UserAuthDtoToUserConverter userAuthDtoToUserConverter;
    private final UserToUserCodeDtoConverter userToUserCodeDtoConverter;
    private final UserToUserActivateDtoConverter userToUserActivateDtoConverter;
    private final UserAuthDtoToEmailDtoConverter userToEmailDtoConverter;
    private final UserAuthDtoToAuthConverter userAuthDtoToAuthConverter;
    private final UserService userService;
    private final AuthService authService;
    private final ClientService clientService;
    private final AuthenticationService authenticationService;
    private final JwtTokenProvider jwtTokenProvider;

    @Override
    public ResponseEntity<DataResponse<UserDto>> register(DataRequest<UserAuthDto> request) {
        log.info("UserConvertServiceImpl -> postEntity -> start");
        log.info("request = {}", request);
        final User entityUser =
                userAuthDtoToUserConverter.convert(request.getData());
        final Auth entityAuth =
                userAuthDtoToAuthConverter.convert(request.getData());
        // Проверим нет ли такой записи в БД
        Optional<Auth> optionalAuth = authService.getByEmail(entityAuth.getEmail());
        if (optionalAuth.isPresent()) {
            log.info("UserConvertServiceImpl такая запись уже существует {}", entityAuth.getEmail());
            return notFoundEntityMessage("Такая запись уже существует", HttpStatus.FOUND);
        }
        // Сохраним пароль
        log.info("UserConvertServiceImpl - запись не найдена {}", entityAuth.getEmail());
        final String password = entityAuth.getPassword();
        entityAuth.setPassword(UserUtils.encryptText(password));
        authService.create(entityAuth);
        // Получим объект auth
        optionalAuth = authService.getByEmail(entityAuth.getEmail());
        if (optionalAuth.isPresent()) {
            // Сохраним пользователя
            entityUser.setCode(UserUtils.generateCode());
            entityUser.setDateAdded(OffsetDateTime.now());
            final Long authId = optionalAuth.get().getId();
            entityUser.setAuthId(authId);
            userService.create(entityUser);
            // Отправим сообщение пользователю по почте
            try {
                log.info("Отправим сообщение пользователю по почте");
                var result = clientService.sendActivateEmail(userToEmailDtoConverter.convert(request.getData()));
                if (result == Result.SUCCESS) {
                    return convertEntityToDtoAndCreateResponse(entityUser, HttpStatus.CREATED);
                } else {
                    return notFoundEntityMessage("Письмо не отправлено", HttpStatus.CREATED);
                }
            } catch (Exception e) {
                log.warn("UserConvertServiceImpl Письмо не отправлено");
                return notFoundEntityMessage("Письмо не отправлено", HttpStatus.CREATED);
            }
        } else {
            log.info("UserConvertServiceImpl такая запись не найдена {}", entityAuth.getEmail());
            return notFoundEntityMessage("Такая запись не найдена", HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<DataResponse<UserActivateDto>> activate(String email) {
        log.info("UserConvertServiceImpl -> activate -> start");
        log.info("request = {}", email);
        // Убедимся что запись существует в БД
        Optional<Auth> optionalAuth = authService.getByEmail(email);
        if (optionalAuth.isPresent()) {
            log.info("UserConvertServiceImpl запись существует {}, активируем", email);
            Optional<User> optionalUser = userService.getByAuthId(optionalAuth.get().getId());
            if (optionalUser.isPresent()) {
                final User entity = optionalUser.get();
                entity.setIsActive(true);
                userService.update(entity);
                log.info("UserConvertServiceImpl -> update -> create--end");
                return convertEntityToActivateDtoAndCreateResponse(entity, HttpStatus.OK);
            } else {
                log.error("UserConvertServiceImpl - запись не найдена {}", email);
                return notFoundEntityMessageActivate("Такая запись не найдена", HttpStatus.NOT_FOUND);
            }
        } else {
            log.info("UserConvertServiceImpl - запись не найдена {}", email);
            return notFoundEntityMessageActivate("Такая запись не найдена", HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<DataResponse<UserDto>> login(DataRequest<UserAuthDto> request) {
        log.info("UserConvertServiceImpl -> postEntity -> start");
        log.info("request = {}", request);
        final String email = request.getData().getEmail();
        final String password = request.getData().getPassword();
        // Найдем такую записи в БД
        Optional<Auth> optionalAuth = authService.getByEmail(email);
        if (optionalAuth.isPresent()) {
            log.info("UserConvertServiceImpl такая запись существует " + email);
            Optional<User> optionalUser = userService.getByAuthId(optionalAuth.get().getId());
            final UserDto userDto = userToUserDtoConverter.convert(optionalUser.get());
            if (password.equals(UserUtils.encryptText(optionalAuth.get().getPassword()))) {
                log.info("UserConvertServiceImpl пароль совпал");
                final HttpHeaders responseHeaders = new HttpHeaders();

                final String token = jwtTokenProvider.createToken(optionalAuth.get().getEmail(),
                        userDto.getRole().getRoleName(),
                        userDto.getNikName());

                userDto.setToken(token);

                // Пока токен будем передавать в объекте
//                responseHeaders.set(HEADER_NAME,
//                        jwtTokenProvider.createToken(optionalAuth.get().getEmail(),
//                                userDto.getRole().getRoleName(),
//                                userDto.getNikName()));

//                первая реалзизация
//                responseHeaders.set(HEADER_NAME,
//                        authenticationService.generateToken(request.getData()));
                return confirmUserRegister(userDto, "Пользователь авторизован",
                        HttpStatus.OK, responseHeaders);
            } else {
                log.error("login({}) -> password incorrect", email);
                return notFoundEntityMessage("Password incorrect: " + email,
                        HttpStatus.NOT_FOUND);
            }
        } else {
            log.error("login({}) -> user not found", email);
            return notFoundEntityMessage("User not found: " + email, HttpStatus.NOT_FOUND);
        }
    }

    // TODO: объединить все эти функции в одну
    private ResponseEntity<DataResponse<UserDto>> convertEntityToDtoAndCreateResponse(
            User entity, HttpStatus created) {
        final UserDto dto = userToUserDtoConverter.convert(entity);
        final DataResponse<UserDto> response = createDataResponse(dto);
        return new ResponseEntity<>(response, created);
    }

    private ResponseEntity<DataResponse<UserAuthDto>> convertEntityToCodeDtoAndCreateResponse(
            User entity, HttpStatus created) {
        final UserAuthDto dto = userToUserCodeDtoConverter.convert(entity);
        final DataResponse<UserAuthDto> response = createDataCodeResponse(dto);
        return new ResponseEntity<>(response, created);
    }

    private ResponseEntity<DataResponse<UserActivateDto>> convertEntityToActivateDtoAndCreateResponse(
            User entity, HttpStatus created) {
        final UserActivateDto dto = userToUserActivateDtoConverter.convert(entity);
        final DataResponse<UserActivateDto> response = createDataActivateResponse(dto);
        return new ResponseEntity<>(response, created);
    }

    // TODO: объединить все эти функции в одну
    private DataResponse<UserDto> createDataResponse(UserDto dto) {
        final DataResponse<UserDto> dataResponse = new DataResponse<>();
        dataResponse.setData(dto);
        return dataResponse;
    }

    private DataResponse<UserAuthDto> createDataCodeResponse(UserAuthDto dto) {
        final DataResponse<UserAuthDto> dataResponse = new DataResponse<>();
        dataResponse.setData(dto);
        return dataResponse;
    }

    private DataResponse<UserActivateDto> createDataActivateResponse(UserActivateDto dto) {
        final DataResponse<UserActivateDto> dataResponse = new DataResponse<>();
        dataResponse.setData(dto);
        return dataResponse;
    }

    protected ResponseEntity<DataResponse<UserActivateDto>> notFoundEntity(String name) {
        final DataResponse<UserActivateDto> response = new DataResponse<>();
        response.setStatus(StatusType.WARNING);
        final String message = "Not found entity by : " + name;
        log.warn(message);
        response.addMessage(HttpStatus.NOT_FOUND.value(), message);
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    protected ResponseEntity<DataResponse<UserAuthDto>> notFoundEntityMessage(String message) {
        final DataResponse<UserAuthDto> response = new DataResponse<>();
        response.setStatus(StatusType.WARNING);
        log.warn(message);
        response.addMessage(HttpStatus.NOT_FOUND.value(), message);
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    protected ResponseEntity<DataResponse<UserDto>> notFoundEntityMessage(
            String message, HttpStatus httpStatus) {
        final DataResponse<UserDto> response = new DataResponse<>();
        response.setStatus(StatusType.WARNING);
        log.warn(message);
        response.addMessage(httpStatus.value(), message);
        return new ResponseEntity<>(response, httpStatus);
    }

    protected ResponseEntity<DataResponse<UserActivateDto>> notFoundEntityMessageActivate(
            String message, HttpStatus httpStatus) {
        final DataResponse<UserActivateDto> response = new DataResponse<>();
        response.setStatus(StatusType.WARNING);
        log.warn(message);
        response.addMessage(httpStatus.value(), message);
        return new ResponseEntity<>(response, httpStatus);
    }

    protected ResponseEntity<DataResponse<UserDto>> confirmUserRegister(
            UserDto userDto, String message, HttpStatus httpStatus, HttpHeaders responseHeaders) {
        final DataResponse<UserDto> response = new DataResponse<>();
        response.setStatus(StatusType.SUCCESSFUL);
        response.setData(userDto);
        response.addMessage(httpStatus.value(), message);
        return new ResponseEntity<>(response, responseHeaders, httpStatus);
    }


    /*
    HttpHeaders responseHeaders = new HttpHeaders();

   responseHeaders.set("MyResponseHeader", "MyValue");
   return new ResponseEntity<String>("Hello World", responseHeaders, HttpStatus.CREATED);
     */


}
