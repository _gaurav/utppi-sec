package ru.alex.auth.service;

import ru.alex.auth.dao.UserDao;
import ru.alex.auth.entity.User;

import java.util.Optional;

public interface UserService extends UserDao {

    Optional<User> getByAuthId(Long authId);

}
