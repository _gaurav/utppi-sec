package ru.alex.auth.service;

import org.springframework.http.ResponseEntity;
import ru.alex.common.dto.auth.UserActivateDto;
import ru.alex.common.dto.auth.UserAuthDto;
import ru.alex.common.dto.auth.UserDto;
import ru.alex.common.dto.data.DataRequest;
import ru.alex.common.dto.data.DataResponse;

public interface UserConvertService {

    ResponseEntity<DataResponse<UserDto>> register(DataRequest<UserAuthDto> request);

    ResponseEntity<DataResponse<UserActivateDto>> activate(String email);

    ResponseEntity<DataResponse<UserDto>> login(DataRequest<UserAuthDto> request);

}
