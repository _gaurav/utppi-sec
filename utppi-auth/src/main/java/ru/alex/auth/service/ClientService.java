package ru.alex.auth.service;

import ru.alex.common.dto.auth.EmailDto;
import ru.alex.common.enums.Result;

public interface ClientService {

    Result sendActivateEmail(EmailDto dto);
}
