package ru.alex.auth.service.Impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.alex.auth.entity.Auth;
import ru.alex.auth.entity.JwtUser;
import ru.alex.auth.entity.User;
import ru.alex.auth.security.JwtUserFactory;
import ru.alex.auth.service.AuthService;
import ru.alex.auth.service.UserService;
import ru.alex.common.dto.auth.UserDto;

import java.util.Optional;

/**
 * Реализация UserDetailsService
 */
@Service
@Slf4j
@AllArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserService userService;
    private final AuthService authService;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<Auth> optionalAuth = authService.getByEmail(email);
        if (optionalAuth.isPresent()) {
            Optional<User> optionalUser = userService.getByAuthId(optionalAuth.get().getId());
            if (optionalUser.isPresent()) {
                final User entity = optionalUser.get();
                JwtUser jwtUser = JwtUserFactory.create(entity, optionalAuth.get());
                log.info("IN loadUserByUsername - user with username: {} successfully loaded", email);
                return jwtUser;
            } else {
                throw new UsernameNotFoundException("User in db: " + email + " not found");
            }
        } else {
            throw new UsernameNotFoundException("User with email: " + email + " not found");
        }
    }
}
