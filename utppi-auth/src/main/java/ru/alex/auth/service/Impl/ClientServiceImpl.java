package ru.alex.auth.service.Impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.alex.auth.config.EmailServiceConfig;
import ru.alex.auth.service.ClientService;
import ru.alex.common.dto.auth.EmailDto;
import ru.alex.common.enums.Result;

@Slf4j
@Service
public class ClientServiceImpl implements ClientService {

    private final RestTemplate restClient;
    private final String emailPath;

    public ClientServiceImpl(RestTemplate restClient, EmailServiceConfig cfg) {
        this.restClient = restClient;
        this.emailPath = String.format("%s://%s:%s%s", cfg.getSchema(),
                cfg.getHost(),
                cfg.getPort(),
                cfg.getPath());
    }

    /**
     * Отправка POST запроса email-сервису на отправку собщения на почту клиента с кодом.
     */
    @Override
    public Result sendActivateEmail(EmailDto dto){
        // TODO: перелать под ResponseEntity<DataResponse<T>>
        log.info("proxyservice -- ClientServiceImpl -- sendActivateEmail --- start ");
        log.info("emailPath  = " + emailPath);
        log.info("EmailDto  = {} ", dto);

        var result = Result.FAIL;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            var httpEntity = new HttpEntity<>(dto, headers);

//            var httpEntity = new HttpEntity<>(dto);

            ResponseEntity<String> response = restClient.exchange(emailPath + "/email/sendmail",
                    HttpMethod.POST, httpEntity
                    , String.class);

            if (response.getStatusCode() == HttpStatus.OK) {
                log.info("HttpStatus.OK");
                result = Result.SUCCESS;
            }
            log.info("sendActivateEmail({0}) -> {}, {1}:{}", dto, result);
        } catch (Exception e){
            e.printStackTrace();
            log.error("sendActivateEmail({}) -> {}", dto, e.getMessage());
            result = Result.FAIL;
        }
        return result;
    }

}
