package ru.alex.auth.service;

import ru.alex.auth.dao.AuthDao;
import ru.alex.auth.entity.Auth;

import java.util.Optional;

public interface AuthService extends AuthDao {

    Optional<Auth> getByEmail(String email);

}
