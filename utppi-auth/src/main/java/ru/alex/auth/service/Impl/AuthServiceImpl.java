package ru.alex.auth.service.Impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.alex.auth.entity.Auth;
import ru.alex.auth.repository.AuthRepository;
import ru.alex.auth.service.AuthService;
import ru.alex.common.dto.data.ListAttributes;
import ru.alex.common.dto.data.ListResult;

import java.util.Map;
import java.util.Optional;

@Slf4j
@AllArgsConstructor
@Service
@Transactional
public class AuthServiceImpl implements AuthService {

    private final AuthRepository authRepository;

    @Override
    public Optional<Auth> get(Long id) {
        return Optional.empty();
    }

    @Override
    public Optional<Auth> getByEmail(String email) {
        log.info("UserServiceImpl -> getByName -> start");
        Optional<Auth> auth =
                authRepository.findAuthByEmail(email);
        // если значение представлено - вернуть его
        if (auth.isPresent()) {
            return auth;
        } else {
            log.info("notFoundEntity");
            return Optional.empty();
        }
    }

    @Override
    public ListResult<Auth> find(ListAttributes listAttributes) {
        return null;
    }

    @Override
    public void create(Auth object) {
        authRepository.save(object);
    }

    @Override
    public Auth create(Long id, Auth object) {
        return null;
    }

    @Override
    public boolean createOrUpdate(Auth object) {
        return false;
    }

    @Override
    public void update(Auth object) {

    }

    @Override
    public boolean update(Auth object, Map<String, Object> data) {
        return AuthService.super.update(object, data);
    }

    @Override
    public boolean delete(Long id) {
        return false;
    }
}
