package ru.alex.auth.service.Impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.alex.auth.entity.Role;
import ru.alex.auth.service.RoleService;
import ru.alex.common.dto.data.ListAttributes;
import ru.alex.common.dto.data.ListResult;

import java.util.Map;
import java.util.Optional;

@Slf4j
@AllArgsConstructor
@Service
@Transactional
public class RoleServiceImpl implements RoleService {




    @Override
    public Optional<Role> get(Long id) {
        return Optional.empty();
    }

    @Override
    public ListResult<Role> find(ListAttributes listAttributes) {
        return null;
    }

    @Override
    public void create(Role object) {

    }

    @Override
    public Role create(Long id, Role object) {
        return null;
    }

    @Override
    public boolean createOrUpdate(Role object) {
        return false;
    }

    @Override
    public void update(Role object) {

    }

    @Override
    public boolean update(Role object, Map<String, Object> data) {
        return RoleService.super.update(object, data);
    }

    @Override
    public boolean delete(Long id) {
        return false;
    }
}
