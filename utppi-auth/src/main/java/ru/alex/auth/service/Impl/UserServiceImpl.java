package ru.alex.auth.service.Impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.alex.auth.entity.Auth;
import ru.alex.auth.entity.User;
import ru.alex.auth.repository.UserRepository;
import ru.alex.auth.service.UserService;
import ru.alex.common.dto.data.ListAttributes;
import ru.alex.common.dto.data.ListResult;

import java.util.Map;
import java.util.Optional;

@Slf4j
@AllArgsConstructor
@Service
@Transactional
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public Optional<User> get(Long id) {
        return Optional.empty();
    }

    @Override
    public Optional<User> getByAuthId(Long authId) {
        log.info("UserServiceImpl -> getByName -> start");
        Optional<User> user =
                userRepository.findUserByAuthId(authId);
        // если значение представлено - вернуть его
        if (user.isPresent()) {
            return user;
        } else {
            log.info("notFoundEntity");
            return Optional.empty();
        }
    }

    @Override
    public ListResult<User> find(ListAttributes listAttributes) {
        return null;
    }

    @Override
    public void create(User object) {
        userRepository.save(object);
    }

    @Override
    public User create(Long id, User object) {
        return null;
    }

    @Override
    public boolean createOrUpdate(User object) {
        return false;
    }

    @Override
    public void update(User object) {
        userRepository.save(object);
    }

    @Override
    public boolean update(User object, Map<String, Object> data) {
        return UserService.super.update(object, data);
    }

    @Override
    public boolean delete(Long id) {
        return false;
    }
}
