package ru.alex.auth.service;

import ru.alex.common.dto.auth.UserAuthDto;

public interface AuthenticationService {

    String generateToken(UserAuthDto user);

}
