package ru.alex.auth.service.Impl;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.springframework.stereotype.Service;
import ru.alex.auth.service.AuthenticationService;
import ru.alex.common.dto.auth.UserAuthDto;

import java.util.Date;

import static ru.alex.common.data.TokenConstants.HEADER_PREFIX;
import static ru.alex.common.data.TokenConstants.EXPIRATION_TIME;
import static ru.alex.common.data.TokenConstants.EMAIL_CLAIM;
import static ru.alex.common.data.TokenConstants.ROLE_CLAIM;
import static ru.alex.common.data.TokenConstants.SECRET_TOKEN;

/**
 * Сервис по работе с токином
 * CreateTokinService
 */
@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    @Override
    public String generateToken(UserAuthDto user) {
        return HEADER_PREFIX + " " + JWT.create()
                .withSubject(user.getId().toString())
                .withIssuedAt(new Date(System.currentTimeMillis()))
                .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .withClaim(EMAIL_CLAIM, user.getEmail())
                .withClaim(ROLE_CLAIM, user.getRole().getRoleName())
                .sign(Algorithm.HMAC512(SECRET_TOKEN));

    }

}
