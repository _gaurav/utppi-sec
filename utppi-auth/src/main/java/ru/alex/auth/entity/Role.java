package ru.alex.auth.entity;

import lombok.*;
import ru.alex.common.sqldb.AbstractIdEntity;

import javax.persistence.*;

/**
 * Класс ролей.
 * @author alex
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "roles", schema = "users")
public class Role extends AbstractIdEntity<Long> {

    // TODO: Заменить на enum
    @Column(name = "role_name", nullable = false)
    private String roleName;
}

