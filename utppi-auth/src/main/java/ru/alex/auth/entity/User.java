package ru.alex.auth.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.alex.common.sqldb.AbstractIdEntity;

import javax.persistence.*;
import java.time.OffsetDateTime;

/**
 * Класс пользователей.
 * @author alex
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "users", schema = "users")
public class User extends AbstractIdEntity<Long> {

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String family;

    // TODO: добавить unique
//    @Column(name = "nik_name", nullable = false, unique = true)
    @Column(name = "nik_name", nullable = false)
    private String nikName;

    @Column(name = "is_active", nullable = false)
    private Boolean isActive;

    @Column(name = "date_added", nullable = false)
    private OffsetDateTime dateAdded;

    @Column(nullable = false)
    private String city;

    @Column(nullable = false)
    private String organization;

    // Код подтверждения
    @Column(nullable = false)
    private Integer code;

    @Column(name = "auth_id", nullable = false)
    private Long authId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "role_id")
    private Role role;

}
