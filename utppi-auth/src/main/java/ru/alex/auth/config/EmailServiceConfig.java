package ru.alex.auth.config;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@NoArgsConstructor
@Configuration
@ConfigurationProperties(prefix = "utppi.email")
public class EmailServiceConfig {
    private String schema;
    private String host;
    private String port;
    private String path;
}
