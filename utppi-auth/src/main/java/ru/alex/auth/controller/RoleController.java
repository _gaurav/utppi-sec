package ru.alex.auth.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.alex.common.data.RestApiConstants;

/**
 * Контроллер обработки всех запрсов касающихся ролей.
 * Возможно этот класс не пригодится
 * @author alex
 */
@Slf4j
//@AllArgsConstructor
//@RestController
@RequestMapping(RoleController.API_URL)
@Tag(name = "Content API", description = "Контроллер обработки всех запрсов касающихся ролей")
public class RoleController {

    protected static final String API_URL = RestApiConstants.API_VERSION_1 + "role";
}
