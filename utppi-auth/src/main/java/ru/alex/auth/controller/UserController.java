package ru.alex.auth.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.alex.common.dto.auth.UserActivateDto;
import ru.alex.common.dto.auth.UserAuthDto;
import ru.alex.auth.service.UserConvertService;
import ru.alex.common.data.RestApiConstants;
import ru.alex.common.dto.auth.UserDto;
import ru.alex.common.dto.data.DataRequest;
import ru.alex.common.dto.data.DataResponse;

/**
 * Контроллер обработки всех запрсов касающихся пользователей.
 * Возможно этот класс не пригодится
 * @author alex
 */
@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping(UserController.API_URL)
@Tag(name = "Content API", description = "Контроллер обработки всех запрсов касающихся ролей")
public class UserController {

    protected static final String API_URL = RestApiConstants.API_VERSION_1 + "user";

    private final UserConvertService userConvertService;

    @Operation(summary = "Регистрация пользователя", description = "Добавление в БД пользователя ")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Пользователь зарегестрирован",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Некорректный запрос",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content) })
    @PostMapping("/register")
    public ResponseEntity<DataResponse<UserDto>> register(@RequestBody DataRequest<UserAuthDto> request) {
        log.info("UserController -> register -> start");
        log.info("UserController request {}", request);
        return userConvertService.register(request);
    }

    @Operation(summary = "Активация пользователя", description = "Активация в БД пользователя")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Активация зарегестрирован",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserActivateDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Некорректный запрос",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content) })
    @PutMapping("/activate/" + RestApiConstants.VARIABLE_EMAIL)
    public ResponseEntity<DataResponse<UserActivateDto>> activate(
            @PathVariable(RestApiConstants.PARAM_EMAIL) String email) {
        log.info("UserController -> activate -> start");
        log.info("UserController email = {}", email);
        return userConvertService.activate(email);
    }

    @Operation(summary = "Авторизация пользователя", description = "Авторизация пользователя, " +
            "формирование токина и отправка его на сервер ")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Пользователь зарегестрирован",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Некорректный запрос",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content) })
    @PostMapping("/login")
    public ResponseEntity<DataResponse<UserDto>> login(@RequestBody DataRequest<UserAuthDto> request) {
        log.info("UserController -> register -> start");
        log.info("UserController request = {}", request);
        return userConvertService.login(request);
    }





    @Operation(summary = "Добавить пользователя", description = "Добавление в БД пользователя ")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Пользователь добавлен",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Некорректный запрос",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content) })
    @PostMapping("/add")
    public ResponseEntity<DataResponse<UserDto>> addUser(@RequestBody DataRequest<UserDto> request) {
        log.info("UserController -> addManufacture -> start");
        log.info("UserController request = {}", request);
//        return manufactureConvertService.postEntity(request);
        return null;
    }


}
