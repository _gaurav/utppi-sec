package ru.alex.auth.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import ru.alex.auth.entity.Auth;
import ru.alex.auth.entity.JwtUser;
import ru.alex.auth.entity.Role;
import ru.alex.auth.entity.User;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Фабрика для создания JwtUser.
 */
public final class JwtUserFactory {
    // TODO: лучше переделать ее в конвертер, непонятно при чем тут фабрика

    public JwtUserFactory() {
    }

    public static JwtUser create(User user, Auth auth) {
        return new JwtUser(
                user.getId(),
                user.getNikName(),
                auth.getPassword(),
                auth.getEmail(),
                user.getIsActive(),
                mapToGrantedAuthorities(user.getRole().getRoleName())
        );
    }

    private static List<GrantedAuthority> mapToGrantedAuthorities(String userRoleName) {
        List<GrantedAuthority> list = new ArrayList<>();
        list.add(new SimpleGrantedAuthority(userRoleName));
        return list;
    }

    /**
     * Вариант для множества ролей у юзера
     * @param userRoles - роль
     * @return
     */
    private static List<GrantedAuthority> mapToGrantedAuthorities(List<Role> userRoles) {
        return userRoles.stream()
                .map(role ->
                        new SimpleGrantedAuthority(role.getRoleName())
                ).collect(Collectors.toList());
    }

}
