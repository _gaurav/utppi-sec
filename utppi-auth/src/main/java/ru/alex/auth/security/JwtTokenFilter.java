package ru.alex.auth.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Этот класс фильтрует запросы и проверяет что там присутсвует токин
 */
@Slf4j
public class JwtTokenFilter extends GenericFilterBean {

    private JwtTokenProvider jwtTokenProvider;

    public JwtTokenFilter(JwtTokenProvider jwtTokenProvider) {
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain)
            throws IOException, ServletException {
        log.info("JwtTokenFilter -> doFilter -> start");

        String token = jwtTokenProvider.resolveToken((HttpServletRequest) req);
        if (token != null && jwtTokenProvider.validateToken(token)) {
            log.info("JwtTokenFilter -> doFilter -> token is valid");
            Authentication auth = jwtTokenProvider.getAuthentication(token);

            log.info("JwtTokenFilter -> doFilter -> auth get");

            if (auth != null) {
                SecurityContextHolder.getContext().setAuthentication(auth);
                log.info("JwtTokenFilter -> doFilter -> auth getContext");
            }
        }
        log.info("JwtTokenFilter -> doFilter -> end");
        filterChain.doFilter(req, res);
    }

}
