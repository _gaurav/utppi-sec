package ru.alex.common.enums;

import lombok.ToString;

/**
 * Тип статуса.
 */
@ToString
public enum StatusType {

    SUCCESSFUL,
    WARNING,
    ERROR;
}
