package ru.alex.common.data;

public final class TokenConstants {

    public static final String EMAIL_CLAIM = "email";
    public static final String ROLE_CLAIM = "role";
    public static final String HEADER_NAME = "utppi_token";
    public static final String HEADER_PREFIX = "Bearer_";
    public static final String SECRET_TOKEN = "b3JhY2xlK29jdG9yeQ";
    public static final Long EXPIRATION_TIME = 864000000L;

}
