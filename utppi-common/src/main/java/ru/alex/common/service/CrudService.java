package ru.alex.common.service;

import ru.alex.common.dto.data.Identifiable;
import ru.alex.common.dto.data.ListAttributes;
import ru.alex.common.dto.data.ListResult;

import java.io.Serializable;
import java.util.Map;
import java.util.Optional;

/**
 * Интерфейс CRUD (create, read, update, and delete) сервиса.
 *
 * @param <T> Тип сущности
 * @param <I> Тип идентификатора опознаваемой сущности
 */
public interface CrudService<T extends Identifiable<I>, I extends Serializable> {

    Optional<T> get(I id);

    ListResult<T> find(ListAttributes listAttributes);

    void create(T object);

    T create(I id, T object);

    boolean createOrUpdate(T object);

    void update(T object);

    default boolean update(T object, Map<String, Object> data) {
        throw new UnsupportedOperationException("Method not implemented");
    }

    boolean delete(I id);
}
