package ru.alex.common.dto.data;

import java.io.Serializable;

/**
 * Списочный фильтр.
 */
public class ListFilter<T extends Serializable> implements Serializable {

    /**
     * Свойство.
     */
    private String property;

    /**
     * Значение.
     */
    private T value;

    /**
     * Оператор.
     */
    private Integer operator;

    public ListFilter() {
    }

    /**
     * Конструктор списочного фильтра.
     *
     * @param property свойство
     * @param value    значение
     */
    public ListFilter(String property, T value) {
        this.property = property;
        this.value = value;
        this.operator = 0;
    }

    /**
     * Конструктор списочного фильтра.
     *
     * @param property свойство
     * @param value    значение
     * @param operator оператор
     */
    public ListFilter(String property, T value, int operator) {
        this.property = property;
        this.value = value;
        this.operator = operator;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public int getOperator() {
        return operator;
    }

    public void setOperator(int operator) {
        this.operator = operator;
    }
}
