package ru.alex.common.dto.auth;

import lombok.*;
import ru.alex.common.dto.data.AbstractId;

import javax.validation.constraints.NotBlank;

/**
 * Роли
 */
@Getter
@Setter
@RequiredArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class RoleDto extends AbstractId<Long> {

    @NotBlank(message = "The \"roleName\" attribute must not be empty")
    private String roleName;

}
