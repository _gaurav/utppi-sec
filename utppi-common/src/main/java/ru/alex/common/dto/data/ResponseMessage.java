package ru.alex.common.dto.data;

import lombok.*;

import java.io.Serializable;

/**
 * Сообщение ответа.
 */
@Getter
@Setter
@RequiredArgsConstructor
@ToString
@EqualsAndHashCode
public class ResponseMessage implements Serializable {

    /**
     * Сообщение.
     */
    private String message;

    /**
     * Код.
     */
    private Integer code;
}
