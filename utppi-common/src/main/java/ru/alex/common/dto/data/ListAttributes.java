package ru.alex.common.dto.data;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Атрибуты списка. Data Transfer Object (DTO).
 */
@Getter
@ToString
@EqualsAndHashCode
public class ListAttributes implements Serializable {

    /**
     * Лимит возвращаемых элементов.
     * Ограничение числа возвращаемых элементов в списке данных.
     */
    private Integer limit;

    /**
     * Смещение возвращаемых элементов.
     * Порядковый номер записи, с которого проводится выборка данных.
     */
    private Integer offset;

    /**
     * Название поля для сортировки. Может быть несколько сортировок, разделяются запятой.
     */
    private String sort;

    /**
     * Список фильтров.
     */
    private List<ListFilter<?>> filters = new ArrayList<>();

    /**
     * Конструктор атрибутов списка. Может быть несколько сортировок, разделяются запятой.
     *
     * @param limit  - лимит возвращаемых элементов
     * @param offset - смещение
     * @param sort   - название поля для сортировки
     */
    public ListAttributes(Integer limit, Integer offset, String sort) {
        this.sort = sort;
        this.limit = limit;
        this.offset = offset;
    }

    /**
     * Конструктор атрибутов списка с сортировкой.
     * Может быть несколько сортировок, разделяются запятой.
     *
     * @param sort - название поля для сортировки
     */
    public ListAttributes(String sort) {
        this(null, null, sort);
    }

    public ListAttributes() {
    }

    /**
     * Добавить списочный фильтр.
     *
     * @param property свойство
     * @param value    значение
     * @param operator оператор
     * @return списочный фильтр
     */
    public <T extends Serializable> ListFilter<T> addFilter(String property, T value, int operator) {
        final ListFilter<T> filter = new ListFilter<>(property, value, operator);
        filters.add(filter);
        return filter;
    }

    public <T extends Serializable> ListFilter<T> addFilter(ListFilter<T> filter) {
        filters.add(filter);
        return filter;
    }
}
