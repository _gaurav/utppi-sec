package ru.alex.common.dto.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Списочный результат данных. Data Transfer Object (DTO).
 *
 * @param <T> Тип полученной из СУБД сущности
 */
public class ListResult<T extends Serializable> implements Serializable {

    /**
     * Общее число сущностей для запроса данных.
     */
    private Integer totalCount;

    /**
     * Число возвращаемых сущностей в списке.
     */
    private Integer count;

    /**
     * Список возвращаемых сущностей.
     */
    private List<T> list = new ArrayList<>();

    public ListResult() {
        super();
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public void addObject(T object) {
        list.add(object);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ListResult)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final ListResult<?> that = (ListResult<?>) o;
        return Objects.equals(totalCount, that.totalCount)
                && Objects.equals(count, that.count)
                && Objects.equals(list, that.list);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), totalCount, count, list);
    }

    @Override
    public String toString() {
        return "ListResult{totalCount=" + totalCount + ", count=" + count + ", "
                + super.toString() + '}';
    }
}
