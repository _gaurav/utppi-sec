package ru.alex.common.dto.data;

import lombok.*;

/**
 * Сессия.
 */
@Getter
@Setter
@RequiredArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Session extends AbstractId<String> {

    /**
     * Идентификатор арендатора.
     */
    private String tenantId;

    /**
     * Идентификатор приложения.
     */
    private String applicationId;

    /**
     * Идентификатор пользователя для сессии.
     */
    private String userId;
}
