package ru.alex.common.dto.data;

import lombok.*;

import java.io.Serializable;

/**
 * Запрос с данными.
 *
 * @param <T> тип данных запроса
 */
@Getter
@Setter
@RequiredArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class DataRequest<T extends Serializable> extends Request {

    /**
     * Данные запроса.
     */
    private T data;
}
