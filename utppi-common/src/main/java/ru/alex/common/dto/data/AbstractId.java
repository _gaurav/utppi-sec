package ru.alex.common.dto.data;

import lombok.*;

import java.io.Serializable;

/**
 * Абстракция - уникальный идентификатор.
 *
 * @param <I> Тип идентификатора опознаваемой сущности
 */
@Getter
@Setter
@RequiredArgsConstructor
@ToString
@EqualsAndHashCode
public abstract class AbstractId<I extends Serializable> implements Identifiable<I> {

    /**
     * Уникальный идентификатор.
     */
    private I id;
}
