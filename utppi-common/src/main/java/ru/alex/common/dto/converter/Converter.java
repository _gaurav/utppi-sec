package ru.alex.common.dto.converter;

public interface Converter<I, O> {

    O convert(I input);

}
