package ru.alex.common.dto.data;

import lombok.*;

@Getter
@Setter
@RequiredArgsConstructor
@ToString
@EqualsAndHashCode
public class Request {

    private MetaInfo metaInfo;
}
