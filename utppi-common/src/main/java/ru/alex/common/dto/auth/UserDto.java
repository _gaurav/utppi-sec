package ru.alex.common.dto.auth;

import lombok.*;
import ru.alex.common.dto.data.AbstractId;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;

/**
 * Пользователи
 */
@Getter
@Setter
@RequiredArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class UserDto extends AbstractId<Long> {

    @NotNull(message = "The \"name\" attribute must not be null")
    private String name;

    @NotNull(message = "The \"family\" attribute must not be null")
    private String family;

    @NotNull(message = "The \"nikName\" attribute must not be null")
    private String nikName;

    @NotNull(message = "The \"nikName\" attribute must not be null")
    private String token;

    @NotNull(message = "The \"isActive\" attribute must not be null")
    private Boolean isActive;

    @NotNull(message = "The \"dateAdded\" attribute must not be null")
    private OffsetDateTime dateAdded;

    @NotNull(message = "The \"city\" attribute must not be null")
    private String city;

    @NotNull(message = "The \"city\" attribute must not be null")
    private String organization;

    @NotBlank(message = "The \"code\" attribute must not be empty")
    private Integer code;

    @NotBlank(message = "The \"authId\" attribute must not be empty")
    private Long authId;

    @NotBlank(message = "The \"role\" attribute must not be empty")
    private RoleDto role;

}
