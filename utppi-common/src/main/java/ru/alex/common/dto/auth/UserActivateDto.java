package ru.alex.common.dto.auth;

import lombok.*;
import ru.alex.common.dto.data.AbstractId;

import javax.validation.constraints.NotNull;

/**
 * Объект Пользователя, подтверждение активации
 */
@Getter
@Setter
@RequiredArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class UserActivateDto extends AbstractId<Long> {

    @NotNull(message = "The \"isActive\" attribute must not be null")
    private Boolean isActive;

}
