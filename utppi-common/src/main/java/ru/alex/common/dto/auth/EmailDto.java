package ru.alex.common.dto.auth;

import lombok.*;
import ru.alex.common.dto.data.AbstractId;

@Getter
@Setter
@RequiredArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class EmailDto extends AbstractId<Long> {
    //    private static final long serialVersionUID = 1L;
    private String to;
    private String subject;
    // TODO: Т.к. теперь html формируем на email сервисе, если эта версия и останется как рабочая
    //  то поле body можно удалить
    private String body;
    private String fullName;
    private Integer code;
}
