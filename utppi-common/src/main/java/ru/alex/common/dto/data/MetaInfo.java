package ru.alex.common.dto.data;

import lombok.*;

import java.util.UUID;

/**
 * Мета информация.
 */
@Getter
@Setter
@RequiredArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class MetaInfo extends AbstractId<String> {

    /**
     * Метка времени.
     */
    private Long timestamp;

    /**
     * Сессия.
     */
    private Session session;

    /**
     * Id канала.
     */
    private UUID channelId;
}
