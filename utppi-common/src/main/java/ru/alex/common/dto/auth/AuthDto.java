package ru.alex.common.dto.auth;

import lombok.*;
import ru.alex.common.dto.data.AbstractId;

import javax.validation.constraints.NotBlank;

/**
 * Пароли
 */
@Getter
@Setter
@RequiredArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class AuthDto extends AbstractId<Long> {

    @NotBlank(message = "The \"email\" attribute must not be empty")
    private String email;

    @NotBlank(message = "The \"password\" attribute must not be empty")
    private String password;

}
