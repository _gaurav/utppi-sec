package ru.alex.common.utils;

import ru.alex.common.exception.IncorrectException;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class UserUtils {

    public static String encryptText(String text) {
        if (text.length() != 0 && text != null) {
            return new String(Base64.getEncoder().encode(text.getBytes(StandardCharsets.UTF_8)));
        } else {
            throw new IncorrectException();
        }
    }

    /**
     * Генератор случайного кода для отправки пользователю.
     * @return код
     */
    public static Integer generateCode(){
        return (int) ((Math.random() * (9999 - 1000)) + 1000);
    }

}
